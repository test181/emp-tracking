package ru.pyshinskiy.emptracking.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pyshinskiy.emptracking.exception.EmployeeNotFoundException;
import ru.pyshinskiy.emptracking.model.Employee;
import ru.pyshinskiy.emptracking.repository.EmpRepository;
import ru.pyshinskiy.emptracking.service.EmpService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class EmpServiceImp implements EmpService {

    @Autowired
    private EmpRepository empRepository;

    @Override
    public Employee get(final String id) throws EmployeeNotFoundException {
        return empRepository.findById(id).orElseThrow(
                EmployeeNotFoundException::new
        );
    }

    @Override
    public List<Employee> getAll() {
        return empRepository.findAll();
    }

    @Override
    @Transactional
    public Employee save(final Employee employee) {
        return empRepository.saveAndFlush(employee);
    }

    @Override
    @Transactional
    public void delete(final String id) {
        empRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteAll() {
        empRepository.deleteAll();
    }
}
