package ru.pyshinskiy.emptracking.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.pyshinskiy.emptracking.enumeration.Sex;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(name = "employee")
public class Employee {

    @Id
    @Column(name = "id")
    private final String id;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "surname")
    private String surname;

    @NotNull
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "birth_date")
    private Date birthDate;

    @NotNull
    @Column(name = "sex")
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @NotNull
    @Column(name = "phone_number")
    private String phoneNumber;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "photo_id", referencedColumnName = "id")
    private Photo photo;

    public Employee() {
        this.id = UUID.randomUUID().toString();
    }

    public Employee(final String name,
                    final String surname,
                    final String middleName,
                    final Date birthDate,
                    final Sex sex,
                    final String phoneNumber) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
    }

    public Employee(final String id,
                    final String name,
                    final String surname,
                    final String middleName,
                    final Date birthDate,
                    final Sex sex,
                    final String phoneNumber) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
    }
}
