package ru.pyshinskiy.emptracking.service;

import ru.pyshinskiy.emptracking.exception.EmployeePhotoNotFoundException;
import ru.pyshinskiy.emptracking.model.Photo;

public interface PhotoService {

    Photo get(String id) throws EmployeePhotoNotFoundException, EmployeePhotoNotFoundException;

    Photo save(Photo employee);

    void delete(String id);
}
