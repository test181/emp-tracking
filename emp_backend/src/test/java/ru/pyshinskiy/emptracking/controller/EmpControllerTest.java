package ru.pyshinskiy.emptracking.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.pyshinskiy.emptracking.dto.EmployeeDTO;
import ru.pyshinskiy.emptracking.dto.converter.DTOConverter;
import ru.pyshinskiy.emptracking.model.Employee;
import ru.pyshinskiy.emptracking.service.EmpService;
import ru.pyshinskiy.emptracking.service.PhotoService;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.pyshinskiy.emptracking.dto.converter.DTOConverter.toEmployeeDTO;
import static ru.pyshinskiy.emptracking.utils.TestUtils.getEmployee;
import static ru.pyshinskiy.emptracking.utils.TestUtils.getEmployees;

@WebMvcTest
@AutoConfigureJsonTesters
public class EmpControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private JacksonTester<EmployeeDTO> employeeDTOJacksonTester;

    @Autowired
    private JacksonTester<List<EmployeeDTO>> employeesDTOJacksonTester;

    @MockBean
    private EmpService empService;

    @MockBean
    private PhotoService photoService;

    @Test
    void getEmployeeReturnsCorrectResult() throws Exception {
        final Employee employee = getEmployee();
        final String employeeDTOJson = getEmployeeDTOJson(employee);
        given(empService.get(employee.getId()))
                .willReturn(employee);
        mvc.perform(get("/employee/" + employee.getId()).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(content().json(employeeDTOJson));
    }

    @Test
    void getEmployeesReturnsCorrectResult() throws Exception {
        final List<Employee> employees = getEmployees();
        final String employeesDTOJson = getEmployeesDTOJson(employees);
        given(empService.getAll()).willReturn(employees);
        mvc.perform(get("/employee").accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(content().json(employeesDTOJson));
    }

    @Test
    void deleteEmployeeReturnsCorrectHttpCode() throws Exception {
        doNothing().when(empService).delete(any(String.class));
        mvc.perform(delete("/employee/id")).andExpect(status().isOk());
    }

    @Test
    void deleteAllEmployeesReturnsCorrectHttpCode() throws Exception {
        doNothing().when(empService).deleteAll();
        mvc.perform(delete("/employee")).andExpect(status().isOk());
    }

    private String getEmployeesDTOJson(final List<Employee> employees) throws IOException {
        return employeesDTOJacksonTester.write(
                employees.stream().map(DTOConverter::toEmployeeDTO).collect(Collectors.toList())
        ).getJson();
    }

    private String getEmployeeDTOJson(final Employee employee) throws IOException {
        return employeeDTOJacksonTester.write(toEmployeeDTO(employee)).getJson();
    }
}

