package ru.pyshinskiy.emptracking.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ru.pyshinskiy.emptracking.enumeration.Sex;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class EmployeeDTO {

    private final String id;

    private String name;

    private String surname;

    private String middleName;

    private Date birthDate;

    private Sex sex;

    private String phoneNumber;
}
