package ru.pyshinskiy.emptracking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.pyshinskiy.emptracking.api.EmpController;
import ru.pyshinskiy.emptracking.dto.EmployeeDTO;
import ru.pyshinskiy.emptracking.dto.PhotoDTO;
import ru.pyshinskiy.emptracking.dto.converter.DTOConverter;
import ru.pyshinskiy.emptracking.exception.EmployeeNotFoundException;
import ru.pyshinskiy.emptracking.exception.EmployeePhotoNotFoundException;
import ru.pyshinskiy.emptracking.model.Employee;
import ru.pyshinskiy.emptracking.model.Photo;
import ru.pyshinskiy.emptracking.service.EmpService;
import ru.pyshinskiy.emptracking.service.PhotoService;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static ru.pyshinskiy.emptracking.dto.converter.DTOConverter.toEmployeeDTO;
import static ru.pyshinskiy.emptracking.dto.converter.DTOConverter.toEmployeeEntity;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("employee")
public class EmpControllerImp implements EmpController {

    @Autowired
    private EmpService empService;

    @Autowired
    private PhotoService photoService;

    @Override
    @GetMapping("{id}")
    public ResponseEntity<EmployeeDTO> get(@PathVariable("id") final String id) throws EmployeeNotFoundException {
        return ResponseEntity.ok(toEmployeeDTO(empService.get(id)));
    }

    @Override
    @GetMapping("{id}/photo")
    public ResponseEntity<PhotoDTO> getEmployeePhoto(
            @PathVariable("id") final String employeeId) throws EmployeePhotoNotFoundException, EmployeeNotFoundException {
        String photoId = empService.get(employeeId).getPhoto().getId();
        Photo photo = photoService.get(photoId);
        return ResponseEntity.ok(new PhotoDTO(photo.getContentType(), photo.getData()));
    }

    @Override
    @GetMapping
    public ResponseEntity<List<EmployeeDTO>> getAll() {
        return ResponseEntity.ok(empService.getAll()
                .stream()
                .map(DTOConverter::toEmployeeDTO)
                .collect(Collectors.toList()));
    }

    @Override
    @PostMapping
    public ResponseEntity<EmployeeDTO> create(
            @RequestPart("employee") final EmployeeDTO employeeDTO,
            @RequestPart("photo") final MultipartFile photoFile
    ) throws IOException {
        Employee employee = toEmployeeEntity(employeeDTO);
        Photo photo = new Photo(photoFile.getContentType(), photoFile.getBytes(), employee);
        employee.setPhoto(photo);
        return ResponseEntity.status(HttpStatus.CREATED).body(
                toEmployeeDTO(empService.save(employee))
        );
    }

    @Override
    @PutMapping
    public ResponseEntity<EmployeeDTO> update(
            @RequestParam(value = "id") final String employeeId,
            @RequestPart(value = "employee", required = false) final EmployeeDTO employeeDTO,
            @RequestPart(value = "photo", required = false) final MultipartFile photoFile
    ) throws IOException, EmployeeNotFoundException, EmployeePhotoNotFoundException {
        final Employee employee = employeeDTO != null ?
                toEmployeeEntity(employeeId, employeeDTO)
                : empService.get(employeeId);
        final Photo photo = photoFile != null ?
                new Photo(photoFile.getContentType(), photoFile.getBytes(), employee)
                : empService.get(employeeId).getPhoto();
        employee.setPhoto(photo);
        return ResponseEntity.status(HttpStatus.OK).body(
                toEmployeeDTO(empService.save(employee))
        );
    }

    @Override
    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable("id") final String id) {
        empService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @DeleteMapping
    public ResponseEntity<?> deleteAll() {
        empService.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
