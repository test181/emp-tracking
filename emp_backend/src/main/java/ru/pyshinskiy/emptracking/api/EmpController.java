package ru.pyshinskiy.emptracking.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import ru.pyshinskiy.emptracking.dto.EmployeeDTO;
import ru.pyshinskiy.emptracking.dto.PhotoDTO;
import ru.pyshinskiy.emptracking.exception.EmployeeNotFoundException;
import ru.pyshinskiy.emptracking.exception.EmployeePhotoNotFoundException;

import java.io.IOException;
import java.util.List;

public interface EmpController {

    ResponseEntity<EmployeeDTO> get(String id) throws EmployeeNotFoundException;

    ResponseEntity<PhotoDTO> getEmployeePhoto(String employeeId) throws EmployeeNotFoundException, EmployeePhotoNotFoundException;

    ResponseEntity<List<EmployeeDTO>> getAll();

    ResponseEntity<EmployeeDTO> create(EmployeeDTO employee, MultipartFile photoFile) throws IOException;

    ResponseEntity<EmployeeDTO> update(
            String employeeId,
            EmployeeDTO employeeDTO,
            MultipartFile photoFile
    ) throws IOException, EmployeeNotFoundException, EmployeePhotoNotFoundException;

    ResponseEntity<?> delete(String id);

    ResponseEntity<?> deleteAll();
}
