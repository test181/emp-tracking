package ru.pyshinskiy.emptracking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.pyshinskiy.emptracking.model.Photo;

@EnableJpaRepositories
public interface PhotoRepository extends JpaRepository<Photo, String> {

}
