package ru.pyshinskiy.emptracking.service;


import ru.pyshinskiy.emptracking.exception.EmployeeNotFoundException;
import ru.pyshinskiy.emptracking.model.Employee;

import java.util.List;

public interface EmpService {

    Employee get(String id) throws EmployeeNotFoundException;

    List<Employee> getAll();

    Employee save(Employee employee);

    void delete(String id);

    void deleteAll();
}
