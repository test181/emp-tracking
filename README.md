# Employee Tracking App
## software
+ Java 8
+ Maven 3.0
+ React 16.13.1
+ npm
## developer
Pavel Pyshinskiy
pavel.pyshinskiy@gmail.com
# Build server App
```
cd emp_backend/ && mvn clean install
```

# Run App
First run server App, then client App
### run server application
```
java -jar emp_backend/target/emp-backend-1.0-SNAPSHOT.jar
```
### run client App
```
cd emp_frontend/ && npm run start
```
