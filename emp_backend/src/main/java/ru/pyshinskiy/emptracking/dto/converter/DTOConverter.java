package ru.pyshinskiy.emptracking.dto.converter;

import ru.pyshinskiy.emptracking.dto.EmployeeDTO;
import ru.pyshinskiy.emptracking.model.Employee;

public class DTOConverter {

    public static Employee toEmployeeEntity(final EmployeeDTO employeeDTO) {
        return new Employee(
                employeeDTO.getName(),
                employeeDTO.getSurname(),
                employeeDTO.getMiddleName(),
                employeeDTO.getBirthDate(),
                employeeDTO.getSex(),
                employeeDTO.getPhoneNumber()
        );
    }

    public static Employee toEmployeeEntity(final String id, final EmployeeDTO employeeDTO) {
        return new Employee(
                id,
                employeeDTO.getName(),
                employeeDTO.getSurname(),
                employeeDTO.getMiddleName(),
                employeeDTO.getBirthDate(),
                employeeDTO.getSex(),
                employeeDTO.getPhoneNumber()
        );
    }

    public static EmployeeDTO toEmployeeDTO(final Employee employee) {
        return new EmployeeDTO(
                employee.getId(),
                employee.getName(),
                employee.getSurname(),
                employee.getMiddleName(),
                employee.getBirthDate(),
                employee.getSex(),
                employee.getPhoneNumber()
        );
    }
}
