package ru.pyshinskiy.emptracking.utils;

import ru.pyshinskiy.emptracking.enumeration.Sex;
import ru.pyshinskiy.emptracking.model.Employee;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestUtils {

    public static Employee getEmployee() {
        return new Employee(
                "Name",
                "Surname",
                "Middle Name",
                new Date(),
                Sex.Male,
                "Phone Number");
    }

    public static List<Employee> getEmployees() {
        return Stream.generate(() -> new Employee(
                "Name",
                "Surname",
                "Middle Name",
                new Date(),
                Sex.Male,
                "Phone Number")
        ).limit(5).collect(Collectors.toList());
    }
}
