package ru.pyshinskiy.emptracking.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PhotoDTO {

    private String contentType;

    private byte[] data;
}
