package ru.pyshinskiy.emptracking.exception;

public class EmployeePhotoNotFoundException extends Exception {

    public EmployeePhotoNotFoundException() {
        super();
    }

    public EmployeePhotoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmployeePhotoNotFoundException(String message) {
        super(message);
    }

    public EmployeePhotoNotFoundException(Throwable cause) {
        super(cause);
    }
}
