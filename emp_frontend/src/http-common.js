import * as axios from "axios";
import {BASE_URL} from "./constants/commonConstants";

export default axios.create({
    baseURL: BASE_URL,
    headers: {
        "Content-type": "application/json"
    }
});