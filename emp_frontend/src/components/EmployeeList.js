import React, { useState, useEffect } from "react";
import EmployeeDataService from "../services/EmployeeService";
import Employee from "./Employee";
import EmployeeForm from "./EmployeeForm";

const EmployeeList = () => {
    const [employees, setEmployees] = useState([]);
    const [currentEmployee, setCurrentEmployee] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);
    const [currentPhoto, setCurrentPhoto] = useState();
    const [editable, setEditable] = useState(false);

    useEffect(() => {
        retrieveEmployees();
    },[]);

    useEffect(() => {
        const retrieveEmployeePhoto = () => {
            EmployeeDataService.getPhoto(currentEmployee.id)
                .then(response => {
                    console.log("Photo has been loaded")
                    console.log(response.data);
                    setCurrentPhoto(response.data);
                })
                .catch(e => {
                    console.log(e);
                });
        };
        if(currentEmployee) {
            retrieveEmployeePhoto();
        }
    }, [currentEmployee])

    const retrieveEmployees = () => {
        EmployeeDataService.getAll()
            .then(response => {
                setEmployees(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    const refreshList = () => {
        retrieveEmployees();
        setCurrentEmployee(null);
        setCurrentIndex(-1);
        setEditable(false);
    };

    const setActiveEmployee = (employee, index) => {
        setCurrentEmployee(employee);
        setCurrentIndex(index);
        console.log(employee.id);
    };

    const removeEmployee = () => {
        EmployeeDataService.deleteById(currentEmployee.id)
            .then(response => {
                console.log(response);
                return refreshList();
            })
            .catch(e => {
                console.log(e);
            })
    }

    const removeAllEmployees = () => {
        EmployeeDataService.removeAll()
            .then(response => {
                console.log(response.data);
                refreshList();
            })
            .catch(e => {
                console.log(e);
            });
    };

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-6">
                    <h4>Employees List</h4>

                    <ul className="list-group">
                        {employees &&
                        employees.map((employee, index) => (
                            <li
                                className={
                                    "list-group-item " + (index === currentIndex ? "active" : "")
                                }
                                onClick={() => {
                                    setActiveEmployee(employee, index)
                                }}
                                key={index}
                            >
                                {employee.name}
                            </li>
                        ))}
                    </ul>
                    <button
                        className="m-3 btn btn-sm btn-danger"
                        onClick={removeAllEmployees}
                    >
                        Remove All
                    </button>
                </div>
                {currentEmployee ? (
                    <div className="container col-md">
                        {editable ? (
                            <EmployeeForm
                                employee={currentEmployee}
                                employeePhoto={currentPhoto}
                                refreshList={refreshList}/>
                        ): (
                            <Employee
                                employee={currentEmployee}
                                employeePhoto={currentPhoto}/>
                        )}
                    </div>
                ): (
                    <div>
                        <br />
                        <p>Please click on a Employee...</p>
                    </div>
                )}
            </div>
            <div className="row">
                <div className="col-md-6"/>
                <div className="col-md">
                    {currentEmployee ? (
                        editable ? (
                            ""
                        ): (
                            <div className="row">
                                <div className="col-md-6">
                                    <button
                                        className="m-3 btn btn-sm btn-info"
                                        onClick={() => setEditable(true)}
                                    >
                                        Edit
                                    </button>
                                </div>
                                <div className="col-md-6">
                                    <button
                                        className="m-3 btn btn-sm btn-danger"
                                        onClick={() => removeEmployee()}
                                    >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        )
                    ) : ("")}
                </div>
            </div>
        </div>
    );
};

export default EmployeeList;