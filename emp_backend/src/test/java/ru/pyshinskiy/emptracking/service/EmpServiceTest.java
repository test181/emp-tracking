package ru.pyshinskiy.emptracking.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.pyshinskiy.emptracking.exception.EmployeeNotFoundException;
import ru.pyshinskiy.emptracking.model.Employee;
import ru.pyshinskiy.emptracking.repository.EmpRepository;
import ru.pyshinskiy.emptracking.service.impl.EmpServiceImp;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static ru.pyshinskiy.emptracking.utils.TestUtils.getEmployee;
import static ru.pyshinskiy.emptracking.utils.TestUtils.getEmployees;

@ExtendWith(MockitoExtension.class)
public class EmpServiceTest {

    @InjectMocks
    private EmpServiceImp empService;

    @Mock
    private EmpRepository empRepository;

    @Test
    void getEmployeeReturnCorrectResult() throws EmployeeNotFoundException {
        final Employee employee = getEmployee();
        given(empRepository.findById(employee.getId())).willReturn(Optional.of(employee));
        assertThat(empService.get(employee.getId())).isEqualTo(employee);
    }

    @Test
    void getEmployeesReturnCorrectResult() {
        final List<Employee> employees = getEmployees();
        given(empRepository.findAll()).willReturn(employees);
        assertThat(empService.getAll()).isEqualTo(employees);
    }
}
