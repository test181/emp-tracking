import React from 'react';
import formatDate from "../util/date-util";

const Employee = ({employee, employeePhoto}) => {

    return (
        <div className="row">
            <div className="col-md-6">
                <h4>Employee</h4>
                <div>
                    <label>
                        <strong>Name:</strong>
                    </label>{" "}
                    {employee.name}
                </div>
                <div>
                    <label>
                        <strong>Surname:</strong>
                    </label>{" "}
                    {employee.surname}
                </div>
                <div>
                    <label>
                        <strong>Middle Name:</strong>
                    </label>{" "}
                    {employee.middleName}
                </div>
                <div>
                    <label>
                        <strong>Birth Date:</strong>
                    </label>{" "}
                    {formatDate(employee.birthDate)}
                </div>
                <div>
                    <label>
                        <strong>Sex:</strong>
                    </label>{" "}
                    {employee.sex}
                </div>
                <div>
                    <label>
                        <strong>Phone Number:</strong>
                    </label>{" "}
                    {employee.phoneNumber}
                </div>
            </div>
            <div className="col-md-6">
                {employeePhoto ? (
                    <img src={`data:image/jpeg;base64,${employeePhoto.data}`}
                         alt="employee"
                         className="img-fluid"/>
                ) : (
                    <div>
                        <p>Loading...</p>
                    </div>
                )}
            </div>
        </div>
    )
}

export default Employee