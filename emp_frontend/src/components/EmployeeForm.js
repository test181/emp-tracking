import React, { useState } from "react";
import EmployeeDataService from "../services/EmployeeService"
import { useHistory } from "react-router-dom";


const EmployeeForm = ({employee, employeePhoto, refreshList}) => {
    const initialEmployeeState = {
        id: null,
        name: "",
        surname: "",
        middleName: "",
        birthDate: "",
        sex: "Male",
        phoneNumber: ""
    };
    const history = useHistory();
    const [editableEmployee, setEditableEmployee] = useState(employee ? employee : initialEmployeeState);
    const [uploadedPhoto, setUploadedPhoto] = useState();

    const handleInputChange = event => {
        const { name, value } = event.target;
        setEditableEmployee({ ...editableEmployee, [name]: value });
    };

    const handlePhotoInputChange = event => {
        const photo = event.target.files[0];
        setUploadedPhoto(photo)
    }

    const saveEmployee = () => {
        const employeeData = {
            name: editableEmployee.name,
            surname: editableEmployee.surname,
            middleName: editableEmployee.middleName,
            birthDate: editableEmployee.birthDate,
            sex: editableEmployee.sex,
            phoneNumber: editableEmployee.phoneNumber
        };

        if(employee) {
            EmployeeDataService.update(employee.id, employeeData, uploadedPhoto)
                .then(response => {
                    setEditableEmployee({
                        id: response.data.id,
                        name: response.data.name,
                        surname: response.data.surname,
                        middleName: response.data.middleName,
                        birthDate: response.data.birthDate,
                        sex: response.data.sex,
                        phoneNumber: response.data.phoneNumber
                    });
                    console.log(response.data);
                    return refreshList(false);
                })
                .catch(e => {
                    console.log(e);
                });
        }
        else {
            EmployeeDataService.create(employeeData, uploadedPhoto)
                .then(response => {
                    console.log(response.data);
                    return history.push("/");
                })
                .catch(e => {
                    console.log(e)
                });
        }
    };

    const getPhotoForPreview = () => {
        return URL.createObjectURL(uploadedPhoto);
    }

    return (
        <div className="submit-form row">
            <div className="col-md-6">
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input
                        type="text"
                        className="form-control"
                        id="name"
                        required
                        value={editableEmployee.name}
                        onChange={handleInputChange}
                        name="name"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="surname">Surname</label>
                    <input
                        type="text"
                        className="form-control"
                        id="surname"
                        required
                        value={editableEmployee.surname}
                        onChange={handleInputChange}
                        name="surname"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="middleName">Middle Name</label>
                    <input
                        type="text"
                        className="form-control"
                        id="middleName"
                        required
                        value={editableEmployee.middleName}
                        onChange={handleInputChange}
                        name="middleName"
                    />
                </div>
                <div>
                    <label htmlFor="birthDate">Birth Date</label>
                    <input
                        type="date"
                        className="form-control"
                        id="birthDate"
                        required
                        value={editableEmployee.birthDate}
                        onChange={handleInputChange}
                        name="birthDate"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="sex">Sex</label>
                    <select
                        className="form-control"
                        id="sex"
                        required
                        value={editableEmployee.sex}
                        onChange={handleInputChange}
                        name="sex">
                        <option>
                            Male
                        </option>
                        <option>
                            Female
                        </option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="phoneNumber">Phone Number</label>
                    <input
                        type="text"
                        className="form-control"
                        id="phoneNumber"
                        required
                        value={editableEmployee.phoneNumber}
                        onChange={handleInputChange}
                        name="phoneNumber"
                    />
                </div>

                <button onClick={saveEmployee} className="btn btn-success">
                    Submit
                </button>
            </div>
            <div className="col-md">
                <div className="form-group">
                    {employeePhoto ? (
                        uploadedPhoto ? (
                            <img src={getPhotoForPreview()} alt="employee" className="img-fluid"/>
                        ) : (
                            <img src={`data:image/jpeg;base64,${employeePhoto.data}`}
                                 alt="employee"
                                 className="img-fluid"/>
                        )
                    ): (
                        <div>
                            <label htmlFor="customFile">Employee Photo</label>
                            {uploadedPhoto ? (
                                <img src={getPhotoForPreview()} alt="employee" className="img-fluid"/>
                            ) : (
                                ""
                            )}
                        </div>
                    )}
                    <div className="custom-file">
                        <input
                            type="file"
                            className="custom-file-input"
                            onChange={handlePhotoInputChange}
                            name="photo"
                            id="customFile"
                        />
                        <label className="custom-file-label" htmlFor="customFile">Choose photo</label>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EmployeeForm;