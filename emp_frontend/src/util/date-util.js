const formatDate = ISODate => {
    return new Date(Date.parse(ISODate)).toLocaleDateString()
}

export default formatDate