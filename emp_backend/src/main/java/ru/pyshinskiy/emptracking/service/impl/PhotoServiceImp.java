package ru.pyshinskiy.emptracking.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pyshinskiy.emptracking.exception.EmployeePhotoNotFoundException;
import ru.pyshinskiy.emptracking.model.Photo;
import ru.pyshinskiy.emptracking.repository.PhotoRepository;
import ru.pyshinskiy.emptracking.service.PhotoService;

import javax.transaction.Transactional;

@Service
public class PhotoServiceImp implements PhotoService {

    @Autowired
    private PhotoRepository photoRepository;

    @Override
    public Photo get(String id) throws EmployeePhotoNotFoundException {
        return photoRepository.findById(id).orElseThrow(
                EmployeePhotoNotFoundException::new
        );

    }

    @Override
    @Transactional
    public Photo save(Photo photo) {
        return photoRepository.saveAndFlush(photo);
    }

    @Override
    @Transactional
    public void delete(String id) {
        photoRepository.deleteById(id);
    }
}
