package ru.pyshinskiy.emptracking.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(name = "photo")
public class Photo {

    @Id
    @Column(name = "id")
    private final String id;

    @Column(name = "content_type")
    private String contentType;

    @Column(name = "data")
    private byte[] data;

    @OneToOne(mappedBy = "photo")
    private Employee employee;

    public Photo() {
        this.id = UUID.randomUUID().toString();
    }

    public Photo(String contentType, byte[] data, Employee employee) {
        this.id = UUID.randomUUID().toString();
        this.contentType = contentType;
        this.data = data;
        this.employee = employee;
    }
}
