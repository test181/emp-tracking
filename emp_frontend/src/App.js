import React from 'react';
import EmployeeList from "./components/EmployeeList";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import EmployeeForm from "./components/EmployeeForm";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
    return (
        <Router>
            <div>
                <nav className="navbar navbar-expand navbar-dark bg-dark">
                    <a href="/employees" className="navbar-brand">
                        Pyshinskiy
                    </a>
                    <div className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to={"/employees"} className="nav-link">
                                Employees
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/add"} className="nav-link">
                                Add
                            </Link>
                        </li>
                    </div>
                </nav>

                <div className="container mt-3">
                    <Switch>
                        <Route exact path={["/", "/employees"]} component={EmployeeList} />
                        <Route exact path="/add" component={EmployeeForm} />
                    </Switch>
                </div>
            </div>
        </Router>
    )
}

export default App;
