import http from "../http-common";
import {BASE_URL} from "../constants/commonConstants";

const getAll = () => {
    return http.get("/employee")
}

const get = id => {
    return http.get(`/employee/${id}`)
}

const getPhoto = employeeId => {
    return http.get(`/employee/${employeeId}/photo`)
}

const create = (employee, photo) => {
    const bodyFormData = new FormData();
    bodyFormData.append("employee", new Blob([JSON.stringify(employee)], {type: "application/json"}));
    bodyFormData.append("photo", photo);
    return http({
        method: "post",
        url: BASE_URL + "/employee",
        data: bodyFormData,
        headers: {"Content-Type": "application/json"}
    })
}

const update = (id, employee, photo) => {
    const bodyFormData = new FormData();
    if(employee) {
        bodyFormData.append("employee", new Blob([JSON.stringify(employee)], {type: "application/json"}));
    }
    if(photo) {
        bodyFormData.append("photo", photo);
    }
    return http({
        method: "put",
        url: BASE_URL + "/employee",
        params: {
            id: id
        },
        data: bodyFormData,
        headers: {"Content-Type": "application/json"}
    })
}

const deleteById = id => {
    return http.delete(`/employee/${id}`)
}

const removeAll = () => {
    return http.delete("/employee")
}

export default {
    get,
    getAll,
    getPhoto,
    create,
    update,
    deleteById,
    removeAll
}